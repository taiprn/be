from database import get_user_by_id


class User:
    def __init__(self, user_id):
        self.id, self.username, self.morale = get_user_by_id(user_id)

    def receive_contact_warning(self):
        return

    def receive_warning(self):
        return self.morale
