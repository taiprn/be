from user import *
from database import login_user

model = 0
users = []


def init_agent():
    global model
    global users
    model = init_ml_model()
    users = {}


def login(user, password):
    user_id = login_user(user, password)
    if user_id:
        users[user_id] = User(user_id)


def warn(user_id):
    return users[user_id].receive_warning()


def warn_contact(user_id):
    return users[user_id].receive_contact_warning()


def evaluate_message(message):
    if model.evaluate(message.contents) < 0:
        for user_id in message.recipients:
            if warn(user_id) < 0:
                warn_contact(user_id)
