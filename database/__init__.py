from .database import init_db, get_user_by_id
'''
called on module import for db connectivity initialization
assures singleton behavior
'''
database.init_db()
